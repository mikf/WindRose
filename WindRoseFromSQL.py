# -*- coding: utf-8 -*-
"""
Created on Thu Aug  2 09:44:39 2018

@author: mikf
"""
import mysql.connector
import pandas as pd
import numpy as np
from windrose import WindroseAxes
import matplotlib.pyplot as plt
from datetime import date, timedelta


def SQLconnect(I):
    cnx = mysql.connector.connect(
        user=I['user'],
        password=I['password'],
        host=I['host'],
        database=I['database'])
    return cnx


def SQLdataframe(cnx, table_name, limit='default', col='*', row='1=1'):
    if limit != 'default':
        string = "SELECT {:s} FROM {:s} WHERE {:s} LIMIT {:d}".format(
                col, table_name, row, limit)
    else:
        string = "SELECT {:s} FROM {:s} WHERE {:s}".format(
                col, table_name, row)
    df = pd.read_sql(
        string,
        con=cnx
        )
    return df


def SQL_read_table(I):
    cnx = SQLconnect(I)
    table_name = I['table_name']
    if 'cols' in I:
        cols = ','.join(I['cols'])
    else:
        cols = '*'
    if 'SQLrowlimit' in I:
        N = I['SQLrowlimit']
    else:
        N = 'default'
    if 'rows' in I:
        rows = I['rows']
        strings = []
        for row in rows:
            string = row + ' LIKE ' + str(rows[row])
            strings.append(string)
        row = ' AND '.join(strings)
    else:
        row = '1=1'
    df = SQLdataframe(cnx, table_name, limit=N, col=cols, row=row)
    cnx.close()
    return df


def get_time_series(start, stop):
    lst = []
    delta = stop - start
    for i in range(delta.days + 1):
        YYYY, MM, DD = str(start + timedelta(i)).split('-')
        for hh in range(24):
            for mm in range(6):
                lst.append(YYYY+MM+DD+'{:02d}{:02d}'.format(hh+1, mm*10))
    print('Number of 10-minute series queried: ' + str(len(lst)))
    return lst


def make_df_pickle(I, lst, pickle_path):
    TIs = []
    wdirs = []
    for name in lst:
        YYYY = name[:4]
        MM = name[4:6]
        I['rows'] = {'Name': name}
        I['table_name'] = 'caldata_{}_{}_50hz'.format(YYYY, MM)
        df = SQL_read_table(I)
        wsp = df.Wsp_44m
        try:
            TI = np.nanstd(wsp)/np.nanmean(wsp)
            wdir = np.mean(df.Wdir_41m)
            TIs.append(TI)
            wdirs.append(wdir)
        except:
            pass
    df_out = pd.DataFrame({'TI': TIs, 'wdir': wdirs})
    df_out.to_pickle(pickle_path)
    return df_out


def get_ts_from_file(file_path):
    with open(file_path, 'r') as f:
        content = f.readlines()
    lst = [c.strip() for c in content]
    print('Number of 10-minute series queried: ' + str(len(lst)))
    return lst


def plot_windrose(df, opt_bar, opt_leg):
    ax = WindroseAxes.from_ax()
    ax.bar(df.wdir, df.TI, **opt_bar)
    ax.set_legend(**opt_leg)
    plt.savefig('Windrose.png')


if __name__ == '__main__':
    if 0:  # time series from start/stop input
        start = date(2017, 12, 29)
        stop = date(2018, 1, 1)
        lst = get_time_series(start, stop)
    else:
        file_path = r"C:\Sandbox\Test\Windrose\ts_list.csv"
        lst = get_ts_from_file(file_path)
    I = {
        'user': 'mikf',
        'host': '10.40.20.14',
        'database': 'v52_wtg',
        'SQLrowlimit': 100,
        'cols': ['Name', 'Wdir_41m', 'Wsp_44m'],
        }
    pickle_path = r"C:\Sandbox\Test\Windrose\data.p"
    if 0:
        I['password'] = input('Password please: ')
        df = make_df_pickle(I, lst, pickle_path)
    else:
        df = pd.read_pickle(pickle_path)
    df.dropna(inplace=True)
    opt_bar = {
            'normed': True,
            'opening': 0.8,
            'edgecolor': 'white',
            }
    opt_leg = {
            'decimal_places': 4,
            'loc': (-0.1, -0.1),
            }
    plot_windrose(df, opt_bar, opt_leg)
